# vim:set fileencoding=utf-8 et ts=4 sts=4 sw=4:
import os
import time

# check if gtk is available
import gi
gi.require_version('Gtk', '3.0')

# This needs to be after the require_version so we get the correct version of
# Gtk # pylint: disable=wrong-import-position
from gi.repository import Gtk  # noqa

from apt_listchanges.frontends import Frontend  # noqa
from apt_listchanges.ALChacks import _  # noqa


class gtk_frd(Frontend):
    def flush_interface(self):
        while Gtk.events_pending():
            Gtk.main_iteration()

    def cb_close(self, widget):
        Gtk.main_quit()

    # pylint: disable=attribute-defined-outside-init
    def show_interface(self):
        """Instantiate and show the window
        Doesn't happen until we are confident we want it to pop up"""
        if self.interface_showing:
            return
        self.interface_showing = True
        self.builder.set_translation_domain("apt-listchanges")
        mydir = os.path.dirname(__file__)
        uipath = os.path.join(mydir, "apt-listchanges.ui")
        self.builder.add_from_file(uipath)
        self.window_main = self.builder.get_object("window_main")
        if self.hide:
            self.window_main.iconify()
        handlers = {
            "on_button_close_clicked": self.cb_close,
            "on_window_main_destroy": self.cb_close,
        }
        self.progressbar_main = self.builder.get_object("progressbar_main")
        self.button_close = self.builder.get_object("button_close")
        self.label_header = self.builder.get_object("label_header")
        self.label_header_descr = self.builder.get_object("label_header_descr")
        self.label_header_descr.hide()
        self.textview_buf = \
            self.builder.get_object("textview_main").get_buffer()

        self.set_title(_('apt-listchanges: Reading changelogs'))
        self.textview_buf.set_text(
            '\n  ' + _('Reading changelogs. Please wait.'))
        self.builder.connect_signals(handlers)
        self.flush_interface()

    def __init__(self, config, *args):
        super().__init__(config, *args)
        self.hide = config.hide
        self.start_time = time.time()
        self.interface_showing = False
        self.deferred_progress = 0.0
        self.builder = Gtk.Builder()

    def display_output(self, text):
        self.show_interface()
        self.label_header_descr.show()
        self.button_close.set_sensitive(True)
        self.textview_buf.set_text(self._render(text))
        self.window_main.set_urgency_hint(True)
        Gtk.main()

    def update_progress(self, diff=1):
        if not self.interface_showing:
            # If we end up not having any content to display, we don't want a
            # window to flash briefly and then disappear just to briefly
            # display a progress bar, so we wait to show the interface until
            # we've been working for a few seconds and it looks like it's
            # going to be at least a few seconds more.
            if time.time() - self.start_time < 2.5 and \
               self.deferred_progress + diff < self.packages_count / 2:
                self.deferred_progress += diff
                return
            self.show_interface()
            self.progress = self.deferred_progress
            self.deferred_progress = 0
        elif not diff:
            return
        if not hasattr(self, 'progress'):
            self.progress = 0.0
        self.progressbar_main.show()
        self.progress += diff
        self.progressbar_main.set_fraction(self.progress / self.packages_count)
        self.progressbar_main.set_text(
            f"{self.progress*100 / self.packages_count}%")
        self.flush_interface()

    def progress_done(self):
        if not self.interface_showing:
            return
        self.progressbar_main.hide()
        self.flush_interface()

    def confirm(self):
        self.show_interface()
        m = Gtk.MessageDialog(self.window_main,
                              Gtk.DialogFlags.MODAL,
                              Gtk.MessageType.QUESTION,
                              Gtk.ButtonsType.YES_NO)
        m.set_default_response(Gtk.ResponseType.YES)
        m.set_markup("<big><b>%s</b></big>\n\n%s" % (
            _("Continue Installation?"),
            _("Select <i>yes</i> to continue with the installation."
              "\nSelect <i>no</i> to abort the installation.")
            ))
        if m.run() == Gtk.ResponseType.NO:
            return False
        return True

    def set_title(self, title):
        self.show_interface()
        self.label_header.set_markup(f'<big><b>{title}</b></big>')
        self.window_main.set_title(title)
